function buttonClicked() {
    let r = document.getElementById("result");

    let price = document.getElementById("price").value;
    if (!(/^(0|[1-9][0-9]*)$/).test(price)) {
        r.innerHTML = "Неверная цена!";
        return;
    }
    price = parseInt(price);

    let amount = document.getElementById("amount").value;
    if (!(/^(0|[1-9][0-9]*)$/).test(amount)) {
        r.innerHTML = "Неверное количество!";
        return;
    }
    amount = parseInt(amount);

    r.innerHTML = "Стоимость: " + price * amount;
}

window.addEventListener("DOMContentLoaded", function () {
    let b = document.getElementById("my-button");
    b.addEventListener("click", buttonClicked);
});